import { AbstractControl, ValidationErrors } from "@angular/forms";

export const passwordsValidator = (control: AbstractControl): ValidationErrors | null => {
    const p1: string = control.get('password')?.value;
    const p2: string = control.get('repeatpassword')?.value;
    const error: ValidationErrors | null = p1 === p2
    ? null
    : { passwordsNotEqual: true };

    control.get('repeatpassword')?.setErrors(error);
    return error;
};