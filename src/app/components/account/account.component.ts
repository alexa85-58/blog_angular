import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { passwordsValidator } from './validators/passwords-validator';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
form: FormGroup = new FormGroup ({

  account: new FormGroup ({
  emailFormControl: new FormControl('', [
   Validators.required,
   Validators.email,
  ]),

  passwordFormControl: new FormControl('',[
   Validators.required,
   Validators.minLength(6)
  ]),

  repeatpasswordFormControl: new FormControl('',[
    Validators.required,
    Validators.minLength(6)
   ]),
  },
  [passwordsValidator]
  ),

  profile: new FormGroup ({
    name: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
  }),

  company: new FormGroup ({
    type: new FormControl('LE', Validators.required),
    inn: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
    kpp: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]),
    okpo: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),  
  }),

  contacts: new FormArray([])
  });

  
  constructor() { 
    
  }

  ngOnInit(): void {
    this.toggleKPPValidator();
  };
  addContacts(): void {
    (this.form.get('contacts') as FormArray).push(
     new FormGroup({
      name: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
    }),
    { emitEvent: false }
  );
  };
  getContactsControls(): AbstractControl[] {
    return (this.form.get('contacts') as FormArray).controls
  }

  toggleKPPValidator(): void {
    this.form.get('company.type')?.valueChanges.subscribe(
     (companyType: string) => {
      if (companyType === 'IE') {
        this.form.get('company.kpp')?.clearValidators();
        this.form.get('company.kpp')?.reset();
        return;
      }
        this.form.get('company.kpp')?.setValidators([
         Validators.required,
         Validators.minLength(9),
         Validators.maxLength(9)
        ])
     }

    );
  }

};
